import React, { Component } from "react";

const implant = (WrappedComponent) => {
  class Mutated extends Component {
    constructor(props) {
      super(props);
      // Setting up state
      this.state = {
        likePostCounter: 0,
      };
    }

    handlePostCount = () => {
      this.setState((prevState) => ({
        likePostCounter: prevState.likePostCounter + 1,
      }));
    };

    render() {
      return (
        <WrappedComponent
          likePostCounter={this.state.likePostCounter}
          handlePostCount={this.handlePostCount}
          {...this.props}
        />
      );
    }
  }

  return Mutated;
};

export default implant;
