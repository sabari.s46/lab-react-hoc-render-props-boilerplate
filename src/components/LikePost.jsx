import React, { useState } from "react";
import implant from "./counterFun";

function LikePost(props) {
  return (
    <div>
      <button onClick={props.handlePostCount}>
        Like Post {props.likePostCounter}
      </button>
    </div>
  );
}

export default implant(LikePost);
