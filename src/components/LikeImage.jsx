import React from "react";
import implant from "./counterFun";

function LikeImage(props) {
  return (
    <div>
      <button onClick={props.handlePostCount}>
        Like Image {props.likePostCounter}
      </button>
    </div>
  );
}

export default implant(LikeImage);
