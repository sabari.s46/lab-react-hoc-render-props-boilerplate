import "./App.css";
import LikeImage from "./components/LikeImage";
import LikePost from "./components/LikePost";
import Counter from "./components/counter";

function App() {
  return (
    <div>
      <h3>Some Blog</h3>
      <div className="buttons">
        <LikeImage />
        <Counter
          render={(count, increment) => (
            <LikePost count={count} increment={increment} />
          )}
        />
      </div>
    </div>
  );
}

export default App;
